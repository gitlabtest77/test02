# test02




##  Требования

- выкачать код проекта
- собрать проект в сборочном образе
- запустить тестирование проекта
- поместить собранный проект в docker-образ (не тот который для сборки а упрощенный который требуется для запуска и работы проекта)
- положить артефакт сборки проекта и docker образ во внешнее хранилище
  - в Package Registry и в Container Registry
- отправить webhook во внешнюю систему о результате завершения работы
  - настроено в setting/webhooks
  - можно настроить в settings/integrations
  - можно добавить вызов curl в конструкции вида
  ```
  after_script:
    - >
      if [ $CI_JOB_STATUS == 'success' ]; then
        echo 'This will only run on success'
      else
        echo 'This will only run when job failed or is cancelled'
      fi
  ```
